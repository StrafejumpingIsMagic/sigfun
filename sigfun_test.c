#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#if defined(__X86_64)
	typedef long int iptr;
#else
	typedef int iptr;
#endif

#define _nop_pad8 asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop");asm("nop")
#define _nop_pad64 _nop_pad8; _nop_pad8; _nop_pad8; _nop_pad8; _nop_pad8; _nop_pad8; _nop_pad8; _nop_pad8
#define _nop_pad512 _nop_pad64; _nop_pad64; _nop_pad64; _nop_pad64; _nop_pad64; _nop_pad64; _nop_pad64; _nop_pad64
#define _nop_pad4k _nop_pad512; _nop_pad512; _nop_pad512; _nop_pad512; _nop_pad512; _nop_pad512; _nop_pad512; _nop_pad512

int main() {
	iptr i = 0;
	while(1) {
		uint8_t* n = malloc(100);
		n[i]++;
		n[i]^=n[i-1]*n[(~(0)) - i];
		n[i]/=i-1;
		_nop_pad4k;
		printf("i == %i\n", i);
		i++;
		free(n);
	}
}
