CC=gcc
INCLUDE_DIRS=-I./
ifeq (${HORROR_LEVEL}, MostHigh)
CFLAGS=-O3 -fPIC -fomit-frame-pointer -DHORROR
else
CFLAGS=-O0 -fPIC -Wall
endif

SIGFUN_SRC=ldasm.c sigfun.c

OBJS=$(SIGFUN_SRC:.c=.o)

%.o:%.c
	${CC} -c ${INCLUDE_DIRS} ${CFLAGS} $< -o $@
libsigfun.so: ${OBJS}
	${CC} ${CFLAGS} -shared ${OBJS} -o libsigfun.so
sigfun_test: libsigfun.so
	${CC} ${CFLAGS} sigfun_test.c -o sigfun_test
	strip -s sigfun_test
clean:
	-rm -v libsigfun.so sigfun_test ${OBJS}
all: libsigfun.so sigfun_test
