#define _GNU_SOURCE
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ldasm.h>

#define _as_bz(...) if ( (__VA_ARGS__) < 0 ) { printf("Failed assert of below zero in %s line %i.\n", __FILE__, __LINE__); }

#if defined(__x86_64)
	typedef long int iptr;
#else
	typedef int iptr;
#endif

void sig_handler(int signum, siginfo_t* si, void* context) {
	mcontext_t* mcontext = &((ucontext_t*)context)->uc_mcontext;
#if defined(__x86_64)
	iptr* pc = (iptr*)&mcontext->gregs[REG_RIP];
	uint8_t* c = (uint8_t*)mcontext->gregs[REG_RIP];
#else
	iptr* pc = (iptr*)&mcontext->gregs[REG_EIP];
	uint8_t* c = (uint8_t*)mcontext->gregs[REG_EIP];
#endif
	ldasm_data ld;
	uint32_t len = 0;
#if defined(__x86_64)
	len = ldasm(c, &ld, 1);
#else
	len = ldasm(c, &ld, 0);
#endif
	if (len != 0 && (ld.flags & F_INVALID) == 0) {
		printf("[sigfun] Signal %i, skipping %i bytes...\n", signum, len);
		*pc += len;
	} else {
		printf("[sigfun] Unrecoverable error at: %lu\n", *pc);
		printf("[sigfun] Exiting: Dissasembly failure.\n");
		exit(1);
	}
}

__attribute__((constructor)) void init(void) {
	struct sigaction a; memset(&a, 0, sizeof(struct sigaction));
	printf("[sigfun] Ready to assist in screwing things up.\n");
	a.sa_sigaction = sig_handler;
	a.sa_flags = SA_SIGINFO;
	_as_bz(sigaction(SIGSEGV, &a, NULL));
	_as_bz(sigaction(SIGFPE, &a, NULL));
	_as_bz(sigaction(SIGHUP, &a, NULL));
	_as_bz(sigaction(SIGABRT, &a, NULL));
}
